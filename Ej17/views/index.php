<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <style>
        label,input
        {
            display:block;
        }
    </style>
</head>
<body>
<?php

    error_reporting(0);

    $nombre = trim(htmlspecialchars($_POST['nombre']));
    $apellido = trim(htmlspecialchars($_POST['apellido']));
    $fecha = trim(htmlspecialchars($_POST['fecha']));
    $email = trim(htmlspecialchars($_POST['email']));

    if($_SERVER['REQUEST_METHOD'] === 'POST'){

        if(!empty($nombre) && !empty($fecha) && !empty($email)){

            echo("<p>Nombre: $nombre</p>");
            echo("<p>Apellido: $apellido</p>");
            if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                echo("<p>El correo electronico no es correcto</p>");
            }else{
            echo("<p>Correo Electrónico: $email</p>");
            }
            if(DateTime::createFromFormat('d/m/Y', $fecha) === false){
                echo("Fecha no valida.<br><br>");
            }else{
                echo("<p>Fecha: $fecha</p><br><br>");
            }
            

        }else{
            echo("Alguno de los campos obligatorios estan vacios.");
        }
    }    
?>
<form action="<?= $_SERVER['PHP_SELF']?>" method="post">
    <label for="nombre">Nombre *</label>
    <input type="text" name="nombre" value="">
    <label for="apellido">Apellido</label>
    <input type="text" name="apellido" value="">
    <label for="email">Correo electrónico *</label>
    <input type="text" name="email" value="">
    <label for="fecha">Fecha de nacimiento *</label>
    <input type="text" name="fecha" value="">
    <input type="submit" value="Enviar">
</form>
</body>
</html>