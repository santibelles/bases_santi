<?php

function esOpcionMenuActiva(string $opcionMenu) : bool{

    if(strpos($_SERVER['REQUEST_URI'], $opcionMenu) !== false)
        return true;

    return false;

}

function obtenerArrayReducido(array &$arrAReducir, int $numElementosReduccion): array{
    shuffle($arrAReducir);

    $chunks = array_chunk($arrAReducir, $numElementosReduccion);

    return chunks[0];
}