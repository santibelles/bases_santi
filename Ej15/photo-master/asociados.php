<?php include __DIR__.'/views/partials/inicio-doc.part.php';?>
<?php require 'utils/utils.php';?>
<?php require 'utils/File.php';?>
<?php require 'entity/Asociado.php';?>
<?php require_once 'exception/FileException.php';?>
<?php require_once 'exception/ValidationException.php';?>
<?php include __DIR__.'/views/partials/nav.part.php';?>

<?php

$errores=[];


try{
            
    if($_SERVER['REQUEST_METHOD']==='POST'){
                
            
        $nombre=trim(htmlspecialchars($_POST['nombre']));
                    
        
                    
        if(empty($nombre)){//SI EL NOMBRE ES VALIDO//REALIZO LAS 
            throw new ValidationException("El nombre no puede quedar vacío");
        }  
        $descripcion=trim(htmlspecialchars($_POST['descripcion']));
            
        $tiposAceptados=['image/jpeg','image/png','image/gif'];
            
        $imagen=new File('logo',$tiposAceptados);
    
            
        $imagen->saveUploadFile(Asociado::RUTA_IMAGEN_ASOCIADOS);
        $asociado=new Asociado($nombre,$imagen->getFileName(),$descripcion);
            
        $mensaje='Se ha guardado asociado'. $asociado->getNombre();
        $descripcion='';
        $nombre='';

        }
    
}catch(FileException $fileException){
    $errores[]=$fileException->getMessage();
}
catch(ValidationException $validationException){
    $errores[]=$validationException->getMessage();
}
?>



<!-- Principal Content Start -->
<?php require 'views/Asociados.view.php';?>

<!-- Principal Content End-->


<?php include __DIR__.'/views/partials/fin-doc.part.php';?>