<?php
require 'utils/utils.php';
require 'views/contact.view.php';

$errores=[];
$mensaje='';
$nombre= '';
$apellido='';
$email='';
$tema='';
$texto='';

    if($_SERVER['REQUEST_METHOD']==='POST'){

        $nombre= trim(htmlspecialchars($_POST['nombre']));
        $apellido= trim(htmlspecialchars($_POST['apellido']));
        $email=trim(htmlspecialchars($_POST['email']));
        $tema=trim(htmlspecialchars($_POST['tema']));
        $texto=trim(htmlspecialchars($_POST['mensaje']));


        if(empty($nombre)){
            $errores[]="El nombre no se puede quedar vacío";
        }
        if(empty($email)){
            $errores[]="El email no se puede quedar vacío";
        }else{
            if(filter_var($email,FILTER_VALIDATE_EMAIL)===false){
                $errores[]="El email no es válido";
            }
        }
        if(empty($tema)){
            $errores[]="El asunto no se puede quedar vacío";
        }
        if(empty($errores)){

            require_once 'database/Connection.php';

            $config=require_once 'app/config.php';

            $connection= Connection::make($config['database']);

            $sql="INSERT INTO mensajes(nombre, apellidos, email, asunto, texto) values('$nombre','$apellido','$email','$tema','$texto');";

            if($connection->exec($sql)===false){
                $errores[]="No se ha podido guardar el mensaje en la base de datos";

            }else{
                $mensaje="Hemos recibido su mensaje";
            }

        }
}

?>