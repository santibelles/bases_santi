<?php
require_once  'entity/ImagenGaleria.php';
require_once  'entity/Asociado.php';
require 'utils/utils.php';
$imagenes=[
    new ImagenGaleria(
            '1.jpg',
            'Descripción imagen 1',
            50,
            0,
            35),
    new ImagenGaleria(
            '2.jpg',
            'Descripción imagen 2',
            50,
            0,
            35),
    new ImagenGaleria(
            '3.jpg',
            'Descripción imagen 3',
            50,
            0,
            35),
    new ImagenGaleria(
            '4.jpg',
            'Descripción imagen 4',
            50,
            0,
            35),
    new ImagenGaleria(
            '5.jpg',
            'Descripción imagen 5',
            50,
            0,
            35),
    new ImagenGaleria(
            '6.jpg',
            'Descripción imagen 6',
            50,
            0,
            35),
    new ImagenGaleria(
            '7.jpg',
            'Descripción imagen 7',
            50,
            0,
            35),
    new ImagenGaleria(
            '8.jpg',
            'Descripción imagen 8',
            50,
            0,
            35),
    new ImagenGaleria(
            '9.jpg',
            'Descripción imagen 9',
            50,
            0,
            35),
    new ImagenGaleria(
            '10.jpg',
            'Descripción imagen 10',
            50,
            0,
            35),
    new ImagenGaleria(
            '11.jpg',
            'Descripción imagen 11',
            50,
            0,
            35),
    new ImagenGaleria(
            '12.jpg',
            'Descripción imagen 12',
            50,
            0,
            35),

];
    $asociados=[
        new Asociado('Primer asociado','log1.jpg','Descripcion primer asociado'),
        new Asociado('Segundo asociado','log2.jpg','Descripcion segundo asociado'),
        new Asociado('Tercer asociado','log3.jpg','Descripcion tercer asociado'),
        new Asociado('Cuarto asociado','log3.jpg','Descripcion cuarto asociado'),
        new Asociado('Quinto asociado','log1.jpg','Descripcion quinto asociado'),
        new Asociado('Sexto asociado','log2.jpg','Descripcion sexto asociado'),
    ];
    $asociados= obtenerArrayReducido($asociados, 3);

    require 'views/index.view.php';



