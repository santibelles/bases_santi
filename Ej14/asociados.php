<?php
require_once 'utils/utils.php';
require_once 'exception/FileException';
require_once 'utils/File.php';
require_once 'entity/Asociado.php';

$errores[]='';

try{
    if ($_SERVER['REQUEST_METHOD']==='POST'){
        $nombre=trim(htmlspecialchars($_post['nombre']));
        if(empty($nombre));

        $descripcion=trim(htmlspecialchars($_post['descripcion']));
        $tipoAceptados=['image/jpeg','image/png','image/gif'];
        $imageFile->saveUploadFile(Asociado::RUTA_IMAGENES_ASOCIADOS);

        $asociado=new Asociado($nombre,$imageFile->getFileName().$descripcion);

        $mensaje="Se ha guardado el asociado $asociado->getNombre()";
        $descripcion='';
        $nombre='';
    }
}catch (FileException $fileException)
{
    $errores[]=$fileException->getMessage();
}
require_once 'views/asociados.view.php';