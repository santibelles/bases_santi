<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 11/11/2018
 * Time: 13:17
 */

class Asociado
{
    const RUTA_IMAGENES_ASOCIADOS ='images/index/asociados/';
private $nombre;

private$logo;

private $descripcion;

    /**
     * Asociado constructor.
     * @param $nombre
     * @param $logo
     * @param $descripcion
     */
    public function __construct($nombre, $logo, $descripcion)
    {
        $this->nombre = $nombre;
        $this->logo = $logo;
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     * @return Asociado
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     * @return Asociado
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     * @return Asociado
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }
    public function getUrlAsociados(){
        return self::RUTA_IMAGENES_ASOCIADOS .$this->getLogo();
    }


}