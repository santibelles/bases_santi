<?php

require_once __DIR__.'/../database/QueryBuilder.php';

class ImagenGaleriaRepository extends QueryBuilder
{

    /**
     * ImagenGaleriaRepository constructor.
     */
    public function __construct(string $table='Imagenes',string $classEntity='ImageGallery')
    {
        parent::__construct($table,$classEntity);
    }

    /**
     * @param ImagenGaleria $imagenGaleria
     * @return Categoria
     * @throws NotFoundException
     * @throws QueryException
     */
    public function getCategoria(ImageGallery $imagenGaleria): Categoria
    {
        $categoriaRepository= new CategoriaRepository();

        return $categoriaRepository->find($imagenGaleria->getCategoria());
    }

    /**
     * @param ImageGallery $imageGallery
     * @throws QueryException
     */
    public function guarda(ImageGallery $imageGallery)
    {
        $fnGuardaImagen=function() use ($imageGallery)
        {
          $categoria=$this->getCategoria($imageGallery);
          $categoriaRepository=new CategoriaRepository();
          $categoriaRepository->nuevaImagen($categoria);
          $this->save($imageGallery);
        };
        $this->executeTransaction($fnGuardaImagen);
    }
}