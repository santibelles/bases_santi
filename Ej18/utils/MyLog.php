<?php

    class MyLog{
        private $log;

        private function __construct(string $filename){
            // create a log channel
            $this->log = new Monolog\Logger('name');
            $this->log->pushHandler(
                new Monolog\Handler\StreamHandler($filename, Monolog\Logger::INFO)
            );

        }


        public static function load(string $filename):MyLog{
            return new MyLog($filename);
        }

        public function add(string $mesagge):void{
            $this->log->addInfo($mesagge);
        }
    }



?>