<?php


$router->get('photo-master', 'app/controllers/index.php');
$router->get('photo-master/about', 'app/controllers/about.php');
$router->get('photo-master/asociados', 'app/controllers/asociados.php');
$router->get('photo-master/blog', 'app/controllers/blog.php');
$router->get('photo-master/contact', 'app/controllers/contact.php');
$router->get('photo-master/imagenes-galeria', 'app/controllers/galeria.php');
$router->get('photo-master/post', 'app/controllers/single_post.php');
$router->post('photo-master/imagenes-galeria/nueva', 'app/controllers/nueva-imagen-galeria.php');

?>