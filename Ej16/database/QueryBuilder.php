<?php

require_once __DIR__. '/../exception/QueryException.php';

class QueryBuilder
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * QueryBuilder constructor.
     * @param PDO $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $table
     * @param string $classEntity
     * @return array
     * @throws QueryException
     */
    public function findAll(string $table, string $classEntity) : array
    {
        $sql="SELECT * from $table";

        $pdoStatement=$this->connection->prepare($sql);


        if($pdoStatement->execute()===false){
                throw new QueryException("No se ha podido ejecutar la query solicitada ");
        }

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $classEntity);
    }
}