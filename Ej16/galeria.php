<?php
require_once 'utils/utils.php';
require_once 'exception/FileException.php';
require_once 'exception/AppException.php';
require_once 'exception/QueryException.php';
require_once 'utils/File.php';
require_once 'entity/ImageGallery.php';
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'core/App.php';

$errores=[];
$descripcion='';
$mensaje='';
$imagenes=[];

try{

    $config=require_once 'app/config.php';
    App::bind('config',$config);

    $connection=App::getConnection();

    if($_SERVER['REQUEST_METHOD']==='POST'){


            $descripcion=trim(htmlspecialchars($_POST['descripcion']));

            $tiposAceptados=['image/jpeg','image/png','image/gif'];
            $imagen=new File('imagen',$tiposAceptados);

            $imagen->saveUploadFile(ImageGallery::RUTA_IMAGENES_GALLERY);
            $imagen->copyFile(ImageGallery::RUTA_IMAGENES_GALLERY,ImageGallery::RUTA_IMAGENES_PORTFOLIO);




            $sql="INSERT INTO Imagenes(nombre, descripcion) VALUES (:nombre ,:descripcion)";



            $pdoStatement=$connection->prepare($sql);


            $parameters=[':nombre'=>$imagen->getFileName(),':descripcion'=>$descripcion];

            if($pdoStatement->execute($parameters)===false){
                $errores[]="No se ha podido guardar la imagen en la base de datos";
            }else{
                $descripcion='';
                $mensaje='Se ha guardado la imagen';
            };





    }
    $queryBuilder= new QueryBuilder($connection);
    $imagenes=$queryBuilder->findAll('Imagenes','ImageGallery');

}
catch(FileException $fileException){

    $errores[]=$fileException->getMessage();
}
catch(QueryException $queryException){
    $errores[]=$queryException->getMessage();
}
catch(AppException $appException){
    $errores[]=$appException->getMessage();
}

require 'views/galeria.view.php';



?>