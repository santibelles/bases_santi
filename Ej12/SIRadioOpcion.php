<?php
/**
 * Created by PhpStorm.
 * User: Adrian
 * Date: 08/11/2018
 * Time: 21:28
 */
require_once 'SelectorIndividual.php';

class SIRadioOpcion extends SelectorIndividual
{

    public function Selector(): string
    {
        $i=0;
        $selector="<label>$this->titulo</label>";
    foreach ($this->elementos as $clave=>$valor){
        if ($this->seleccionado ===$i)
            $selecionado='checked';
        else
            $selecionado='';
        $selector .= sprintf(
            "<label><input type='radio' name='%s' values='%s'%s>%s</label>",
            $this->nombre,
            $clave,
            $selecionado,
            $valor
        );
        $i++;
    }
    return $selector;
    }
}